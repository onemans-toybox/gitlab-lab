# GitlabLab

I referenced the code from this website https://github.com/reg-viz/reg-suit/ 's reg-notify-gitlab-plugin

If you want to run this code, you should make `.env` file in root directory.

```env
GITLAB_HOST=https://gitlab.com
GITLAB_TOKEN=xxxxxxxxxxxxxxxxxxxxxxx
```
