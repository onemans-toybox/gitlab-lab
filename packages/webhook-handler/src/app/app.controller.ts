import { Controller, Get, Post, Req } from '@nestjs/common';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getData() {
    return this.appService.getData();
  }

  @Post('mergerequest-event')
  postMergerequestEvent(@Req() request: Request) {

    console.log('================================================================');
    console.log(request.body);
    console.log('================================================================');

    return this.appService.getData();
  }

  @Post('push-event')
  postPushEvent(@Req() request: Request) {
    return this.appService.getData();
  }

}
