import { nodeGitlog } from './node-gitlog';

describe('nodeGitlog', () => {
  it('should work', async () => {
    process.chdir('../Something');

    const result = await nodeGitlog({ since: '2023-01-01' });
    console.log(result);

    expect('node-gitlog').toEqual('node-gitlog');
  });

  it('args validation check 1', async () => {
    process.chdir('../Something');

    try {
      const result = await nodeGitlog({ since: new Date().toLocaleString() });
      console.log(result);
    } catch (err) {
      if (
        err instanceof Error &&
        err.message === 'Invalid date format. Use YYYY-MM-DD'
      ) {
        expect(true).toEqual(true);
        return;
      }
    }

    expect(true).toEqual(false);
  });

  it('args validation check 2', async () => {
    process.chdir('../Something');

    try {
      const result = await nodeGitlog({ since: new Date().toLocaleString() });
      console.log(result);
    } catch (err) {
      if (
        err instanceof Error &&
        err.message === 'Invalid date format. Use YYYY-MM-DD'
      ) {
        expect(true).toEqual(true);
        return;
      }
    }

    expect(true).toEqual(false);
  });

  it('args validation check 3', async () => {
    process.chdir('../');

    try {
      const result = await nodeGitlog({ since: '2023-01-01' });
      console.log(result);
    } catch (err) {
      if (
        err instanceof Error &&
        err.message === 'Invalid date format. Use YYYY-MM-DD'
      ) {
        expect(true).toEqual(true);
        return;
      }
      console.error(err);
    }

    expect(true).toEqual(false);
  });
});
