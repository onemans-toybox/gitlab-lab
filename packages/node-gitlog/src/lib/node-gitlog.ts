import { exec } from 'child_process';

type Context = {
  since: string;
};

type ReturnType = {
  hash: string;
  message: string;
};

export async function nodeGitlog({ since }: Context): Promise<ReturnType> {
  const dateFormat = /^\d{4}-\d{2}-\d{2}$/;

  // Argument Validation
  if (isNaN(new Date(since).getTime()) || !dateFormat.test(since)) {
    throw new Error('Invalid date format. Use YYYY-MM-DD');
  }

  return new Promise((resolve, reject) => {
    exec(
      `git log --pretty=format:'{"hash": "%H", "message": "%s"}' --merges --since=${since}`,
      (error, stdout) => {
        if (error) {
          reject(`exec error: ${error}`);
          return;
        }
        const json: ReturnType = JSON.parse(
          `[${stdout.trim().replace(/\n/g, ',')}]`
        );
        resolve(json);
      }
    );
  });
}
