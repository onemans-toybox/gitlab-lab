import { Gitlab } from '@gitbeaker/node';
import addDiscussionToMergeRequests from './functions/add-discussion-to-merge-requests';

import addDraftToMergeRequestIfNeeded from './functions/add-draft-to-merge-request-if-needed';
import commentToMergeRequests from './functions/comment-to-merge-requests';
import removeDraftToMergeRequestIfNeeded from './functions/remove-draft-to-merge-request-if-needed';

type Context = {
  host: string;
  token: string;
};

function MergeRequestHandler({ host, token }: Context) {
  const api = new Gitlab({
    host,
    token,
  });

  return {
    addDraftToMergeRequestIfNeeded: (
      context: Parameters<typeof addDraftToMergeRequestIfNeeded>[1]
    ) => {
      return addDraftToMergeRequestIfNeeded(api, context);
    },
    removeDraftToMergeRequestIfNeeded: (
      context: Parameters<typeof removeDraftToMergeRequestIfNeeded>[1]
    ) => {
      return removeDraftToMergeRequestIfNeeded(api, context);
    },
    commentToMergeRequests: (
      context: Parameters<typeof commentToMergeRequests>[1]
    ) => {
      return commentToMergeRequests(api, context);
    },
    addDiscussionToMergeRequests: (
      context: Parameters<typeof addDiscussionToMergeRequests>[1]
    ) => {
      return addDiscussionToMergeRequests(api, context);
    },
  };
}

export default MergeRequestHandler;
