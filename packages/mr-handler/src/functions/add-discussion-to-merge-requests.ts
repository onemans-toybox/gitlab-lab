import { Gitlab } from '@gitbeaker/node';
import { COMMENT_MARK } from '../config/comment-mark';

type Context = ContextBase & (ContextByCommitId | ContextByMergeRequestId);

type ContextBase = {
  projectId: number;
  message: string;
  resolve?: boolean;
};

type ContextByCommitId = {
  type: 'commitId';
  targetCommitId: string;
};

type ContextByMergeRequestId = {
  type: 'mergeRequestId';
  mergeRequestIid: number;
};

function createNoteBody(message: string) {
  return COMMENT_MARK + '\n' + message;
}

async function addDiscussionToMergeRequestsByCommitId(
  api: InstanceType<typeof Gitlab>,
  {
    projectId,
    targetCommitId,
    message,
    resolve = false,
  }: ContextBase & ContextByCommitId
) {
  try {
    const mrList = await api.MergeRequests.all({ projectId });

    if (!mrList.length) {
      throw new Error(
        `There's no opened merge requests. Retry open some merge request including the commit ${targetCommitId}`
      );
    }

    const commitsList = await Promise.all(
      mrList.map(async (mr) => {
        const commits = await api.MergeRequests.commits(projectId, mr.iid);
        return { mr, commits };
      })
    );

    const targetMrs = commitsList.filter(({ commits }) =>
      commits.some((c) => c.id === targetCommitId)
    );
    if (!targetMrs.length) {
      throw new Error(
        `There's no opened merge requests including the commit ${targetCommitId} ...`
      );
    }

    await Promise.all(
      targetMrs.map(async ({ mr }) => {
        const discussions = await api.MergeRequestDiscussions.all(
          projectId,
          mr.iid
        );
        const commentedDiscussion = discussions.find(
          (discussion) =>
            discussion.notes.find((note) =>
              note.body.startsWith(COMMENT_MARK)
            ) !== undefined
        );
        if (!commentedDiscussion) {
          await api.MergeRequestDiscussions.create(
            projectId,
            mr.iid,
            createNoteBody(message)
          );
        } else {
          await api.MergeRequestDiscussions.editNote(
            projectId,
            mr.iid,
            commentedDiscussion.id,
            commentedDiscussion.notes[0].id,
            {
              body: createNoteBody(message),
            }
          );
        }

        if (resolve) {
          await api.MergeRequestDiscussions.resolve(
            projectId,
            mr.iid,
            commentedDiscussion.id,
            true
          );
        }
      })
    );
  } catch (err) {
    console.error(err);
    throw err;
  }
}

async function addDiscussionToMergeRequestsByMergeRequestIid(
  api: InstanceType<typeof Gitlab>,
  {
    projectId,
    mergeRequestIid,
    message,
    resolve = false,
  }: ContextBase & ContextByMergeRequestId
) {
  try {
    const discussions = await api.MergeRequestDiscussions.all(
      projectId,
      mergeRequestIid
    );
    const commentedDiscussion = discussions.find(
      (discussion) =>
        discussion.notes.find((note) => note.body.startsWith(COMMENT_MARK)) !==
        undefined
    );
    if (!commentedDiscussion) {
      await api.MergeRequestDiscussions.create(
        projectId,
        mergeRequestIid,
        createNoteBody(message)
      );
    } else {
      await api.MergeRequestDiscussions.editNote(
        projectId,
        mergeRequestIid,
        commentedDiscussion.id,
        commentedDiscussion.notes[0].id,
        {
          body: createNoteBody(message),
        }
      );
    }

    if (resolve) {
      await api.MergeRequestDiscussions.resolve(
        projectId,
        mergeRequestIid,
        commentedDiscussion.id,
        true
      );
    }
  } catch (err) {
    console.error(err);
    throw err;
  }
}

async function addDiscussionToMergeRequests(
  api: InstanceType<typeof Gitlab>,
  context: Context
) {
  if (context.type === 'commitId') {
    await addDiscussionToMergeRequestsByCommitId(api, context);
  } else if (context.type === 'mergeRequestId') {
    await addDiscussionToMergeRequestsByMergeRequestIid(api, context);
  }
}

export default addDiscussionToMergeRequests;
