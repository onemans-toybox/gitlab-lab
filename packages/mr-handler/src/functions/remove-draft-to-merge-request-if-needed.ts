import { Gitlab } from '@gitbeaker/node';

/**
 * 指定プロジェクトの指定MRをドラフトからレディー状態にする
 * すでにレディーになっている場合は変更処理を実施しない
 */
async function removeDraftToMergeRequestIfNeeded(
  api: InstanceType<typeof Gitlab>,
  {
    projectId,
    mrId,
  }: {
    projectId: number;
    mrId: number;
  }
) {
  const mr = await api.MergeRequests.show(projectId, mrId);

  // タイトルにドラフト指定がされている場合のみ処理を実行する
  // Draftの定義は https://docs.gitlab.co.jp/ee/user/project/merge_requests/drafts.html 参照
  let draftType: 'Draft:' | '[Draft]' | null = null;

  if (mr.title.startsWith('Draft:')) {
    draftType = 'Draft:';
  }

  if (mr.title.startsWith('[Draft]')) {
    draftType = '[Draft]';
  }

  if (draftType !== null) {
    await api.MergeRequests.edit(projectId, mrId, {
      title: mr.title.replace(draftType, ''),
    });
  }
}

export default removeDraftToMergeRequestIfNeeded;