import { Gitlab } from '@gitbeaker/node';

/**
 * 指定プロジェクトの指定MRをドラフト指定にする。
 * すでにドラフトになっている場合は変更処理を実施しない
 */
async function addDraftToMergeRequestIfNeeded(api: InstanceType<typeof Gitlab>, {
    projectId,
    mrId,
  }: {
    projectId: number;
    mrId: number;
  }) {
    const mr = await api.MergeRequests.show(projectId, mrId);
  
    // すでにタイトルにドラフト指定がされている場合は以降の処理をスキップして終了する
    // Draftの定義は https://docs.gitlab.co.jp/ee/user/project/merge_requests/drafts.html 参照
    if (mr.title.startsWith('[Draft]') || mr.title.startsWith('Draft:')) {
      return;
    }
  
    await api.MergeRequests.edit(projectId, mrId, {
      title: `[Draft]${mr.title}`,
    });
  }

  export default addDraftToMergeRequestIfNeeded