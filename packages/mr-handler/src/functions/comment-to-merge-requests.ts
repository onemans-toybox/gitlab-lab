import { Gitlab } from '@gitbeaker/node';
import { COMMENT_MARK } from '../config/comment-mark';

type Context = {
  projectId: number;
  targetCommitId: string;
  message: string;
};

function createNoteBody(message: string) {
  return COMMENT_MARK + '\n' + message;
}

async function commentToMergeRequests(
  api: InstanceType<typeof Gitlab>,
  { projectId, targetCommitId, message }: Context
) {
  try {
    const mrList = await api.MergeRequests.all({ projectId });
    if (!mrList.length) {
      // OpenなMRが存在しない
      return;
    }
    const commitsList = await Promise.all(
      mrList.map(async (mr) => {
        const commits = await api.MergeRequests.commits(projectId, mr.iid);
        return { mr, commits };
      })
    );
    const targetMrs = commitsList.filter(({ commits }) =>
      commits.some((c) => c.id === targetCommitId)
    );
    if (!targetMrs.length) {
      // 対象のコミットを含むMRが存在しない
      return;
    }
    await Promise.all(
      targetMrs.map(async ({ mr }) => {
        try {
          const notes = await api.MergeRequestNotes.all(projectId, mr.iid);
          const commentedNote = notes.find((note) =>
            note.body.startsWith(COMMENT_MARK)
          );
          if (!commentedNote) {
            await api.MergeRequestNotes.create(
              projectId,
              mr.iid,
              createNoteBody(message)
            );
          } else {
            await api.MergeRequestNotes.edit(
              projectId,
              mr.iid,
              commentedNote.id,
              createNoteBody(message)
            );
          }
        } catch (err) {
          console.error(err);
          throw err;
        }
      })
    );
  } catch (err) {
    console.error(err);
    throw err;
  }
}

export default commentToMergeRequests;
