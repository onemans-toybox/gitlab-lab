import * as dotenv from 'dotenv';
import MergeRequestHandler from './main';

dotenv.config();

const gitLabHost = process.env['GITLAB_HOST'];
const gitLabToken = process.env['GITLAB_TOKEN'];

const _sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

describe('MergeRequestHandler', () => {
  it('test', async () => {
    const handler = MergeRequestHandler({
      host: gitLabHost,
      token: gitLabToken,
    });

    // テスト環境にあわせて変更する
    const projectId = 42420488;
    const targetCommitId = '142775966d1081d577bcc98aa2b440101972573c';

    await handler.addDiscussionToMergeRequests({
      type: 'commitId',
      projectId,
      targetCommitId,
      message: ':white_circle: :white_circle: :white_circle:',
    });

    await _sleep(5000);

    await handler.addDiscussionToMergeRequests({
      type: 'commitId',
      projectId,
      targetCommitId,
      message: ':red_circle: :white_circle: :white_circle:',
    });

    await _sleep(5000);

    await handler.addDiscussionToMergeRequests({
      type: 'commitId',
      projectId,
      targetCommitId,
      message: ':red_circle: :red_circle: :white_circle:',
    });

    await _sleep(5000);

    await handler.addDiscussionToMergeRequests({
      type: 'commitId',
      projectId,
      targetCommitId,
      message: ':red_circle: :red_circle: :red_circle:',
      resolve: true,
    });
  }, 30000);
});
